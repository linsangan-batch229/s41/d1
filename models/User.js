const mongoose = require('mongoose');

//[USERS SCHEMA]
const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'firstname is required!']
    },
    lastName: {
        type: String,
        required: [true, 'lastname is required!']
    },
    email: {
        type: String,
        required: [true, 'email is required!']
    },
    password: {
        type: String,
        required: [true, 'Password is required!']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNumber: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    enrollments: [{
        courseId: {
            type: String,
            required: [true, "Course ID is required"]
        },
        enrolledOn: {
            type: Date,
            default: new Date()
        },
        status : {
            type: String,
            default: "Enrolled"
        }
    }]

})

//[USER MODEL]
module.exports = mongoose.model("User", userSchema);