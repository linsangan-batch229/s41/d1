const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js');
const auth = require("../auth.js")


//check email if it existing to our database
router.post("/checkEmail", (req, res) => {
    userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//user registration route
router.post("/register", (req, res) => {
    userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


//User Login w/ show auth
router.post("/login", (req, res) => {
    userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Retrieving user details
//request.params - kung saan pwede pa kumuha ng id maliban sa reqBody
//auth.verify - middle to ensure that the user is logged in
//before they can enroll to a course
router.post("/details", auth.verify, (req, res) => {
    // uses the decode method define in the auth.js to retrieve user
    //info from request header
    const userData = auth.decode(req.headers.authorization)

    userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
})


//      [ENROLL A USER]
router.post("/enroll", auth.verify, (req, res) => {

    //values needed to enroll, one for the user who will enroll, one for the
    //course that they would like to enroll in
    let data = {
        userId: auth.decode(req.headers.authorization).id,
        courseId : req.body.courseId
    }

    userControllers.enroll(data).then(resultFromController => res.send(resultFromController));
})



// para maging global si router
//available sa ibang files
module.exports = router;