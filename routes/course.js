const express = require('express');
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js")

//crete course
router.post("/", auth.verify, (req, res) => {

    //retrieve the all the data from user
    const userData = auth.decode(req.headers.authorization)
    
    //get the isAdmin key if its T/F
    if(userData.isAdmin){
        courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));

    } else {
        res.send(`Failed to create a course. You're not an admin.`)
    }
});


//      [RETRIEVE ALL COURSES]
router.get("/all", (req, res) => {
    courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
})


//      [RETRIEVE ALL ACTIVE COURSES]
// create a route that will retrieve all active endpoints: "/"
//no login needed
// create a controller that will return all activecourses
//
router.get("/", (req, res) => {
    courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})


//      [RETREIVE SPECIFIC COURSE]
//  :courseId - wildcard
router.get("/:courseId", (req, res) => {
    courseControllers.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})



//      [UPDATE A COUSE] 
//      [2 PARAMETER, PARAMS and BODY]
router.put("/:courseId", auth.verify, (req, res) => {
    courseControllers.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


//      [SOFT DELETE]
router.put("/courseId/:archive", auth.verify, (req, res) => {
    courseControllers.archive(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

//

//export the router object for index.jsfil
module.exports = router;