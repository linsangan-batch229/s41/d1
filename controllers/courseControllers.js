const Course = require("../models/Course.js");
const auth = require('../auth.js');
const Users = require("../models/User.js");

//create a new course
/*
Steps:
1. create a new Course object using the mongosoe model
2. save the new Course to the database 
*/

module.exports.addCourse = (reqBody) => {

    //craetes a new variable "newCourse and instantiate a new  Course
    //object
    //uses the information from the reqbody to provide all necessary
    //info

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })
 
    //saves the create object to our database
    return newCourse.save().then((course, error) => {
        // if failed
        if (error){
            return false;
        //if success
        } else {
            return `A new course has been created "${reqBody.name}."`;
        }
    })
}


//      [RETRIEVE ALL COURSE]
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
}



//      [RETREIVE ALL ACTIVE COURSE]
module.exports.getAllActive = () => {
    return Course.find({isActive : true }).then(result => {
        return result
    })
}

//      [RETRIEVE SPEICIFC COURSE]
// localhost:4000/courses/63c8c909fb03ac2c6dae8567
module.exports.getCourse = (reqParams) => {
    console.log(reqParams);
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}


//      [UPDATE A COURSE]
/*

STEPS:
1. create a variable "updatedCouse" which will obtain the info retrieved from the req body
2. find and update the course using tthe course ID from the req 
params and the variable "updatedCourse"

*/

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    //findByUpdate(document ID, updatesToBeApllied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

        //course not updated
        if (error){
            return false;

         // course updated successfully   
        } else {
            return true;
        }
    })
}


module.exports.archive = (reqParams, reqBody) => {
    let itemToArchive = {
        isActive : reqBody.isActive
    };

    return Course.findByIdAndUpdate(reqParams.archive, itemToArchive).then((course, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    })

}
