//[SERVER PREPARE]
const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require('./routes/course.js');

const app = express();

// [MONGODB  CONNECTION]
mongoose.connect('mongodb+srv://admin:admin1234@cluster0.2bdmdsh.mongodb.net/s37-s41?retryWrites=true&w=majority', {
    useNewUrlParser : true,
    useUnifiedTopology  : true
});


let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// [Middleware]
app.use(express.json());
app.use(express.urlencoded( {extended: true}));
//default url for use
app.use("/users", userRoutes);
//default url for couse
app.use("/courses", courseRoutes);

// [LISTEN]
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
})